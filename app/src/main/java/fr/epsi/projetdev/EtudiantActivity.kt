package fr.epsi.projetdev

import android.content.Context
import android.content.Intent

import android.os.Bundle


import android.widget.TextView

class EtudiantActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_etudiant)
        val getIntent:Intent = intent


        setTitle(getIntent.getStringExtra("nom")+" "+getIntent.getStringExtra("prenom"))

        val textNomPrenom: TextView = findViewById(R.id.nomPrenomEtudiant)
        val textEmail: TextView = findViewById(R.id.emailEtudiant)
        val groupeEtudiant: TextView = findViewById(R.id.groupEtudiant)



        textNomPrenom.text = getIntent.getStringExtra("nom")+" "+getIntent.getStringExtra("prenom")
        textEmail.text = getIntent.getStringExtra("email")
        groupeEtudiant.text = getIntent.getStringExtra("groupe")



    }





    companion object{
        fun startEtudiantActivity(con: Context, nom:String,prenom:String,email: String,groupe:String){
            con.startActivity(Intent( con,EtudiantActivity::class.java)
                .putExtra("nom",nom)
                .putExtra("prenom",prenom)
                .putExtra("email",email)
                .putExtra("groupe",groupe))







        }
    }
}
