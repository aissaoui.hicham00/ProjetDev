package fr.epsi.projetdev


import android.os.Bundle

import android.content.Context
import android.content.Intent


class CategorieActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categorie)
    }
    companion object {
        fun startCategorieActivity(con: Context) {
            con.startActivity(Intent(con, CategorieActivity::class.java))
        }
    }
}