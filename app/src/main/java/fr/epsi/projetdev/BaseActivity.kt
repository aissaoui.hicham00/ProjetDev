package fr.epsi.projetdev

import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

open class BaseActivity: AppCompatActivity() {
    fun setTitle(Title:String){
        val title: TextView = findViewById(R.id.testid1)
        title.setText(Title)
    }
}