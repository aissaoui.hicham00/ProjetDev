package fr.epsi.projetdev

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button


class GroupActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group)
        val button1:Button = findViewById(R.id.Etudiant1)
        val button2:Button = findViewById(R.id.Etudiant2)
        val button3:Button = findViewById(R.id.Etudiant3)
        setTitle("Infos")

        button1.setOnClickListener(View.OnClickListener {
            EtudiantActivity.startEtudiantActivity(application,"AISSAOUI"
                ,"Hicham"
                ,"hicham.aissaoui@epsi.fr"
                ,"G2")


        })
    }

    companion object{
        fun startGroupActivity(con: Context){
            con.startActivity(Intent(con,GroupActivity::class.java))
        }
    }
}